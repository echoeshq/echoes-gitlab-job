Hello there :wave: [**Echoes**](https://echoeshq.com) **Echoes lets you express the motivation behind merge requests simply through GitLab labels.** We surface this data into dashboards that shine a light on your work and help management understand the reality of the activity.

<div align="center">
<img src="http://app.echoeshq.com/echoes_bow.png" width="200" />
</div>

We **don't** surface any individualized data, nor performance metrics, nor scores. The goal is to help organizations set clearer goals, improve focus, and offer a better context overall for engineers to deliver their best work. This starts by giving managers an accurate understanding of the reality of the teams. 

## What does it change for developers?

**TL;DR: use the labels prefixed with `echoes/` to tag your merge requests.** That's all! No need for additional tickets, time-tracking, or switching to a different tool.

These labels are centrally defined in [Echoes](https://echoeshq.com) to represent what matters to _your_ organization, and we keep them in sync across all of your repositories so you don't have to.

| Label prefix | Purpose |
| ------------------------------- | ------------- |
| `echoes/intent: `      | Used to express _why_ the pull request exists |
| `echoes/initiative: ` | Used to express to _what_ initiative (e.g., feature or project) the pull request contributes to |
| `echoes/effort: `      | Used to express _how much_ effort went into the pull request (defaults to `M`) |

**[Echoes](https://echoeshq.com) requires pull requests to be tagged with _at least_ one intent or initiative, and _optionally_ with the effort that went into it.**

## FAQ

**Q: Can I add a new `echoes/` label from GitLab if needed?**
No: Echoes will regularly reconcile labels on repositories with its configuration and your label will get deleted. New labels must be added from [Echoes configuration](https://app.echoeshq.com/settings/) and will then get published on all repositories.

**Q: Will it make any difference on results if I send fewer/more, or smaller/larger pull requests?**
No: Echoes doesn't look at the diff, nor at the absolute number of pull requests. You can continue contributing exactly as you're used to, with the minor change of adding labels!
