# echoes-gitlab-job

CI job running **only** on Merge Requests and check on Echoes Labels.
It creates a [detached pipeline](https://docs.gitlab.com/ee/ci/pipelines/merge_request_pipelines.html).

The job can be relaunched manually after an update of the `labels`.

To know more about Echoes and its labels please have a look at [NOTES.md](./NOTES.md)

## Usage

In your `.gitlab-ci.yml` file:

- add the include `echoes-check.yml`
- add `echoes:check` to the defined `stages`


```diff
+ include: https://gitlab.com/echoeshq/echoes-gitlab-job/raw/main/echoes-check.yml

stages:
  - deploy
+ - echoes:check

deploy-job:      
  stage: deploy
  script:
    - echo "Application successfully deployed."
```

